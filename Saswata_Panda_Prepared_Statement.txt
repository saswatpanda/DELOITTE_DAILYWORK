Name - Saswata Panda
Topic -Prepare Statement
----------------

- PreparedStatement in Java is one of several ways to execute SQL queries using JDBC API. 

- Java provides Statement,PreparedStatement and CallableStatement for executing queries. Out of these three, 
  Statement is used for general purpose queries, PreparedStatement is used for executing parametric query and   CallableStatement is used for executing Stored Procedures. 

- PreparedStatement is a class in java.sql package and allows Java programmer to execute SQL queries by using JDBC   package. You can get PreparedStatement object by calling connection.prepareStatement() method.

- PreparedStatement allows you to write dynamic and parametric query.
  By using PreparedStatement in Java you can write parametrized sql queries and send different parameters by using same   sql queries which is lot better than creating different queries.

- PreparedStatement is faster than Statement in Java.
  One of the major benefits of using PreparedStatement is better performance. PreparedStatement gets pre compiled
  In database and there access plan is also cached in database, which allows database to execute parametric query   written using prepared statement much faster than normal query because it has less work to do. You should always try   to use PreparedStatement in production JDBC code to reduce load on database.

- PreparedStatement prevents SQL Injection attacks in Java.
   In SQL Injection attack, malicious user pass SQL meta-data combined with input which allowed them to execute sql    query of there choice, If not validated or prevented before sending query to database. By using parametric queries    and PreparedStatement you prevent many forms of SQL injection because all the parameters passed as part of place-   holder will be escaped automatically by JDBC Driver.

- PreparedStatement query return FORWARD_ONLY ResultSet, so you can only move in one direction Also concurrency level of   ResultSet would be "CONCUR_READ_ONLY".

- All JDBC Driver doesn't support pre compilation of SQL query in that case query is not sent to database when you call   prepareStatement(..) method instead they would be sent to database when you execute PreparedStatement query.

- Despite of being very useful PreparedStatement also has few limitations.
  In order to prevent SQL Injection attacks in Java, PreparedStatement doesn't allow multiple values for one placeholder  (?) who makes it tricky to execute SQL query with IN clause.

   
    eg.
      import java.sql.*;  
	class InsertPrepared{  
	public static void main(String args[]){  
	try{  
	Class.forName("oracle.jdbc.driver.OracleDriver");  
  
	Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","oracle");  
  
	PreparedStatement stmt=con.prepareStatement("insert into Emp values(?,?)");  
	stmt.setInt(1,101);//1 specifies the first parameter in the query  
	stmt.setString(2,"Ratan");  
  
	int i=stmt.executeUpdate();  
	System.out.println(i+" records inserted");  
  
	con.close();  
  
	}catch(Exception e){ System.out.println(e);}  
  
	}  
	}  









