Binding: If the compiler knows which function to call 

-Static : Compile  time 
-Dynamic: Run time


Constructor
-It is a managerial function
-Doesn't have a return type(not even void)
-If a return type is specified, it becomes a method with the name similar to that of the class 
-Gets invoked when the object is created automatically, only once in a life time, i.e, it can not be re-invoked(can't be invoked after object creation).
-It has the same name(CASE SENSITIVE) of the class
-Constructors can be overloaded(Strongly Recomended).
-Initialises instance variables
-Mainly used to return object pointers.
-In Java classes, an object can not be created if there is no constructor. Hence, if the user does not specify/declare a constructor, the compiler creates a default constructor of zero arguments.
- If user declares a constructor, the default constructor will not be created by the compiler. Hence, the user should create the object with the existing constructor or a default constructor should explicitly be created.
-A construcor can invoke only one other constructor using 'super' or 'this' keyword.(Limitation : It always should be the first statement).
-A constructor can invoke method, but a method can not call a constructor.
-A default constructor has a keyword called 'super' which is a call to the super class construcor.
-Java supports copy constructor*

*Copy constructor copies the data of one object of a class to another object of the same class.

-Since Java supports anonymous classes(without a name), the constructor for anonymous class is the anonymous block.
-However, we can have an anonymous block and constructor in a named class. In this case, anonymous block will execute first and then the constructor. If there are more than one anonymous blocks, top to bottom all the anonymous blocks will be executed.

There are two types of anonymous blocks.
-Instance(Works for every object)
-Static/Class(higher precedence than Instance)(Works only once through-out the application)

Create a class for integer array with following specifications
-If the array size is not specified, then 10;
-It should be able to adopt another array;
-It should have a copy constructor
-A method to display horizontal

Create a class to display area of 
- IF  Class can take no arguments, ojbect undefined;
- If 3 parameter, triangle
-If 2 parameter, rectangle
-If 1f circle
-If 1 square

GARBAGE COLLECTION:

DISPOSE TECHNIQUE: Specifying the destrucotr to release all the resources(related to OS), before the goes for garbage collection.

DEMON THREAD: A background process(not user defined)

ENCAPSULATION:


DESTRUCTORS
-A Managerial method that gets executed just before the Garbage collection.


		| Same Class | Same Package | Outside Package
----------------|------------|--------------|------------------
Private		|	Y    |	     N	    |        N		
----------------|------------|--------------|------------------
Default		|	Y    |	     Y	    |        N		
----------------|------------|--------------|------------------
Protected	|	Y    |	     Y	    |        N,Y(If Inherited)		
----------------|------------|--------------|------------------
Public		|	Y    |	     Y	    |        Y		


When a class is public and its constructor is private, the class members and methods can be used everywhere, but object creation is not possible outside the class. However object reference can be accesssed everywhere.

When a class is public and its constructor is default, the class members and methods can be used everywhere, but object creation is not possible outside the package.However object reference can be accesssed everywhere.





Types Of Objects -

- Bean Objects(java bean , Enterprise java Bean)/POJO/Value objects/Transfer Objects

          They have only data members.



2. Business Objects/Transfer objects
     They have only the logic but not the data members values i.e they are independent of data

3. DAO(Data Acess Object)

                   Read/write data to/from

Q: Create a class for following
    1. Addition
    2. Substraction
    3. Multiplication
    4. Division
 Each class should have num1, num2, num3 .
 Get , set, cal, Disp

Lhs() = rhs (Tightly Coupled)
lhs(super class) = rhs(Subclass)


Inheritance


- Reusability
- Abstract class is a pure virtual class




Create a java class to play card game with 4 players.


Overriding :

   same name same signature different class